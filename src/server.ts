import { GraphQLServer } from "graphql-yoga";
import handler from "./handler";

const server = new GraphQLServer(handler)
server.start(() => console.log("Server is running on localhost:4000"))