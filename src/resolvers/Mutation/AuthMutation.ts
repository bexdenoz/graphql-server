import { GraphQLResolveInfo } from "graphql";

interface ILoginArgs {
    username: String;
    password: String;
}


export const login = async (_: any, args: ILoginArgs, ctx: any) => {
    return {
        type: "Login",
        success: false
    };
};

interface ISignupArgs {
    username: String;
    password: String;
}

export const signup = async (_: any, args: ISignupArgs, ctx: any) => {
    return {
        type: "Signup",
        success: false
    };
};

export const updateProfile = async (_: any, args: any, ctx: any, info: GraphQLResolveInfo) => {
    return {
        fullName: args.fullName,
    };
};