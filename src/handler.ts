// import {
//     // graphql,
//     // buildSchema,
//     // GraphQLAbstractType,
//     // GraphQLArgs,
//     // GraphQLResolveInfo
// } from "graphql";
// import { importSchema } from "graphql-import";

import Query from "./resolvers/Query";
import Mutation from "./resolvers/Mutation";

// const schema = buildSchema(importSchema("./src/schemas/schema.graphql"));

// Root resolver
const resolvers = {
  Query,
  Mutation
  // Query: {
  //   ping: () => "PONG",
  // },
  // Mutation: {
  //   async login (usr: String, pwd: String) {
  //     return {
  //       type: "Login",
  //       success: false
  //     }
  //   },
  //   async signup (usr: String, pwd: String) {
  //     return {
  //       type: "Signup",
  //       success: false
  //     }
  //   },
  //   async updateProfile(parent: any, args: any, ctx: any, info: GraphQLResolveInfo) {
  //     // console.log("Mutation", fullName, ctx, root)
  //     return {
  //       fullName: args.fullName,
  //     }
  //   }
  // },
  // Profile: {
  //   fullName: () => "test"
  // }
};

// const query = `mutation {
//     updateProfile(fullName: "Ahmed") {
//         fullName
//     }
// }`;

// graphql(schema, query, root, {a:2}, null).then(result => {
//     console.log(result);
// });

export default {
  typeDefs: "./src/schemas/schema.graphql",
  resolvers
};
